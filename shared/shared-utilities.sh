#!/usr/bin/env bash

TIGERVNC_LINK="https://dl.bintray.com/tigervnc/stable/"
BASE_JAVA="https://www.oracle.com"
JAVA_DOWNLOAD_PAGE="/technetwork/java/javase/downloads/index.html"

# Determines where the output of commands should be piped
# @param $1 - boolean flag to indicate verbosity, meaning output is discarded
# @return String containing the location where command output should be piped
function determine_output() {
  if [[ "$1" == true ]] ; then
    echo "/dev/tty"
  else
    echo "/dev/null"
  fi
}

##
# Connects to the TigerVNC SourceForge and pulls the latest version number for your bit type
# @param $1 -  String where command output will be sent
# @return Name of the package to download
##
function get_tigervnc_version() {
  local output=$(determine_output $1)
  local temp_file="/tmp/tigervnc_version.tmp"

  safe_download ${output} ${temp_file} https://sourceforge.net/projects/tigervnc/files/stable/
  sed -i '/\/projects\/tigervnc\/files\/stable\//!d' ${temp_file}
  sed -i '/<th/!d' ${temp_file}
  sed -E -i 's/.*([0-9]+\.[0-9]+\.[0-9]+).*/\1/' ${temp_file}
  echo $(sort -n -t '.' -k 2 ${temp_file} | tail -1 && rm -f ${temp_file})
}

# Creates the tiger vnc init.d service
# @param $1 - String where command output will be sent
# @param $2 - Name of the user to run the VNC under
# @param $3 - String containing the name of the operating system (CentOS, Debian or Ubuntu)
# @return None
function setup_vnc_initd_service() {
  output=$1
  name=$2
  operating_system=$3

  safe_download ${output} "/etc/init.d/vncserver" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/tigervnc/vncserver-initd.service
  sed -i "s/user_name/$name/g" /etc/init.d/vncserver
  chmod +x /etc/init.d/vncserver
  if [[ ${operating_system} == "centos" ]]; then
    chkconfig vncserver on &> ${output}
  else
    update-rc.d vncserver defaults
  fi

  service vncserver start &> ${output}
}

##
# Creates the tiger vnc SystemD service
# @param $1 - String where command output will be sent
# @param $2 - Name of the user to run the VNC under
# @return None
##
function setup_vnc_systemd_service() {
  output=$1
  name=$2

  safe_download ${output} "/etc/systemd/system/vncserver@.service" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/tigervnc/vncserver@.service
  sed -i "s/user_name/$name/g" /etc/systemd/system/vncserver@.service
  chmod +x /etc/systemd/system/vncserver@.service

  systemctl daemon-reload &> ${output}
  systemctl enable  vncserver@:1 &> ${output}
  systemctl start  vncserver@:1 &> ${output}
}

# Parses the JDK downloads page to find the download link
# @param $1 - String where command output will be sent
# @param $2 - Bit type of the operating system as int, 32 or 64
# @param $3 - File extension (rpm or tar.gz)
# @return The download link for the JDK
function get_jdk_download_link() {
  output=$1
  if [[ $2 -eq 32 ]] ; then
    echo "https://cdn.azul.com/zulu/bin/zulu15.32.15-ca-jdk15.0.3-linux_i686.tar.gz"
  else
    echo "https://github.com/AdoptOpenJDK/openjdk15-binaries/releases/download/jdk-15.0.2%2B7/OpenJDK15U-jdk_x64_linux_hotspot_15.0.2_7.tar.gz"
  fi
}

# Sets up the configuration files for Openbox, Fbpanel and PCManFM
# @param $1 - boolean flag to indicate whether or not to run the function in verbose mode
# @param $2 - Name of the user account to setup
function setup_desktop() {
  output=$(determine_output $1)
  name=$2

  mkdir -p /home/$name/.config/openbox &> $output
  mkdir -p /home/$name/.config/fbpanel &> $output
  mkdir -p /home/$name/.config/pcmanfm/default &> $output

  safe_download ${output} "/home/$name/.config/openbox/autostart" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/desktop/openbox-autostart.txt
  safe_download ${output} "/home/$name/.config/fbpanel/default" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/desktop/fbpanel-default-config.txt
  safe_download ${output} "/home/$name/.config/pcmanfm/default/desktop-items-0.conf" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/desktop/pcmanfm-desktop-items.txt
  safe_download ${output} "/home/$name/.config/pcmanfm/default/pcmanfm.conf" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/desktop/pcmanfm-default-config.txt
  safe_download ${output} "/home/$name/.gtkrc-2.0" https://bitbucket.org/teamfluffee/fluffees-server-setup/raw/master/shared/desktop/gtk-settings.txt
  sed -i "s/user_name/$name/g" /home/$name/.gtkrc-2.0
  chown -R ${name}:${name} /home/${name}/*
  chown -R ${name}:${name} /home/${name}/.*
  update-alternatives --install /usr/bin/x-file-manager x-file-manager /usr/bin/pcmanfm 100 &> $output
  update-alternatives --install /usr/bin/x-terminal x-terminal /usr/bin/xterm 100 &> $output
  update-alternatives --install /usr/bin/x-www-browser x-www-browser /usr/bin/firefox 100 &> $output
  dbus-uuidgen > /var/lib/dbus/machine-id
}

# Creates bot folder, downloads TRiBot and OpenOSRS
# @param $1 - boolean flag to indicate whether or not to run the function in verbose mode
# @param $2 - Name of account where bots should be installed
# @return - None
function setup_bots() {
  output=$(determine_output $1)
  name=$2

  mkdir /home/$name/Desktop/ &> $output

  install_tribot_15 $output $name
  install_graphical_client_starter_loader ${output} ${name}
  download_openosrs $output $name
}

# Installs TRiBot 15
# @param $1 - boolean flag indicating whether or not we want to run in verbose
# @param $2 - Name of the account being used
# @return - None
function install_tribot_15() {
  output=$1
  name=$2

  local tribot_icon_path="/home/${name}/.tribot/tribot-icon.ico"

  mkdir -p /home/${name}/.tribot &> ${output}
  safe_download ${output} "tribot-splash.jar" https://runeautomation.com/downloads/tribot/portable
  mv tribot-splash.jar /home/${name}/.tribot/tribot-splash.jar
  safe_download ${output} "tribot-icon.ico" https://s3-forums-files.tribot.org/monthly_2019_01/favicon.ico.3801e13d62963562430c0b50f2026d05.ico
  mv tribot-icon.ico /home/${name}/.tribot/tribot-icon.ico

  touch "/home/${name}/.tribot/run_tribot.sh"
  echo "#!/usr/bin/env bash" >> /home/${name}/.tribot/run_tribot.sh
  echo "" >> /home/${name}/.tribot/run_tribot.sh
  echo "java -jar /home/${name}/.tribot/tribot-splash.jar" >> /home/${name}/.tribot/run_tribot.sh

  chmod +x /home/${name}/.tribot/run_tribot.sh

  mkdir -p /home/${name}/.local/share/applications/
  touch /home/${name}/.local/share/applications/TRiBot.desktop

  echo "[Desktop Entry]" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Encoding=UTF-8" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Version=1.0" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Type=Application" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Terminal=false" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Exec=/home/${name}/.tribot/run_tribot.sh" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Name=TRiBot" >> "/home/${name}/.local/share/applications/TRiBot.desktop"
  echo "Icon=${tribot_icon_path}" >> "/home/${name}/.local/share/applications/TRiBot.desktop"

  cp "/home/${name}/.local/share/applications/TRiBot.desktop" "/home/${name}/Desktop"
}

function install_graphical_client_starter_loader() {
  output=$1
  name=$2

  local download_link=$(get_latest_github_release ${output} "https://github.com/Naton1/Graphical-Client-Starter/releases/latest" ".jar")
  local loader_location="/home/${name}/Desktop/Graphical_Client_Starter.jar"
  local settings_location="/home/${name}/.tribot/Graphical Client Starter/settings"

  safe_download ${output} ${loader_location} "${download_link}"
  chmod +x ${loader_location}

  mkdir -p "${settings_location}"
  touch "${settings_location}/last.json"

  echo "{" >> "${settings_location}/last.json"
  echo "  \"customTribotPath\": {" >> "${settings_location}/last.json"
  echo "    \"value\": \"/home/${name}/.tribot/tribot-splash.jar\"" >> "${settings_location}/last.json"
  echo "  }" >> "${settings_location}/last.json"
  echo "}" >> "${settings_location}/last.json"
}

# Downloads the OpenOSRS launcher from their Github by parsing the latest release page to find the newest version
# @param $1 - boolean flag to indicate whether or not to run the function in verbose mode
# @param $2 - Name of account where bots should be installed
# @return - None
function download_openosrs() {
  name=$2

  mkdir -p /home/${name}/.local/bin/openosrs/

  local download_link=$(get_latest_github_release ${output} "https://github.com/open-osrs/launcher/releases/latest" ".jar")

  safe_download ${output} "OpenOSRS.jar" ${download_link}
  mv "OpenOSRS.jar" "/home/${name}/Desktop/OpenOSRS.jar"
  chmod +x "/home/${name}/Desktop/OpenOSRS,jar"
  chown ${name} "/home/${name}/Desktop/OpenOSRS.jar"
}

function get_latest_github_release() {
  output=$1
  github_project_link=$2
  file_ending=$3

  local temp_file="/tmp/github_output.tmp"

  safe_download ${output} ${temp_file} ${github_project_link}
  sed -i '/'"${file_ending}"'/!d' ${temp_file}
  sed -i '/href/!d' ${temp_file}
  download_link_ending=$(cat ${temp_file} | sed -e 's/.*href=\"\(.*\)\"\ rel=.*/\1/')
  rm ${temp_file}
  echo "https://github.com/${download_link_ending}"
}

# Creates shortcuts on the desktop to allow quickly changing VNC resolution
# @param $1 - Name of the user with the desktop to create the shortcuts on
# @return - None
function create_resolution_change() {
  name=$1

  mkdir -p "/home/$name/Desktop/Change-Screen-Resolution"
  chown $name "/home/$name/Desktop/Change-Screen-Resolution"
  echo 'xrandr -s 640x480' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-640x480.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 800x600' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-800x600.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1024x768' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1024x768.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1280x720' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1280x720.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1280x800' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1280x800.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1280x960' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1280x960.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1280x1024' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1280x1024.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1360x768' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1360x768.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1400x1050' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1400x1050.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1680x1050' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1680x1050.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1680x1200' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1680x1200.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1920x1080' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1920x1080.sh"
  echo -e '#!/usr/bin/env bash\n\nxrandr -s 1920x1200' >> "/home/$name/Desktop/Change-Screen-Resolution/Change-to-1920x1200.sh"
  chmod -R 754 "/home/$name/Desktop/Change-Screen-Resolution/"
}

# Allows jar files to be double clicked to run
# @param $1 - Name of the user with the desktop to create the shortcuts on
# @return - None
function enable_jar_doubleclick() {
  name=$1
  java_directory=$(readlink -f /etc/alternatives/java)

  echo "[Desktop Entry]" >> JB-java-jdk8.desktop
  echo "Encoding=UTF-8" >> JB-java-jdk8.desktop
  echo "Name=Oracle Java 8 Runtime" >> JB-java-jdk8.desktop
  echo "Comment=Oracle Java 8 Runtime" >> JB-java-jdk8.desktop
  echo "Exec=${java_directory} -jar %f" >> JB-java-jdk8.desktop
  echo "Terminal=false" >> JB-java-jdk8.desktop
  echo "Type=Application" >> JB-java-jdk8.desktop
  echo "Icon=oracle_java8" >> JB-java-jdk8.desktop
  echo "MimeType=application/x-java-archive;application/java-archive;application/x-jar;" >> JB-java-jdk8.desktop
  echo "NoDisplay=false" >> JB-java-jdk8.desktop

  mv JB-java-jdk8.desktop /usr/share/applications/JB-java-jdk8.desktop
  mkdir -p /home/$name/.local/share/applications

  echo "[Added Associations]" >> /home/$name/.local/share/applications/mimeapps.list
  echo "application/x-java-archive=JB-java-jdk8.desktop;" >> /home/$name/.local/share/applications/mimeapps.list
}

##
# Gets the download link (from SourceForge) for the latest version of TigerVNC
# @param $1 - String representing where the call outputs should be sent
# @param $2 - The system's architecture bit type as a number
# @return - The download link for the latest TigerVNC as a string
##
function get_tigervnc_download_link() {
  local output=$(determine_output $1)
  if [[ "$2" -eq 64 ]] ; then
    local architecture="x86_64"
  else
    local architecture="i386"
  fi

  local base_link="https://iweb.dl.sourceforge.net/project/tigervnc/stable/"
  local tigervnc_version=$(get_tigervnc_version $output)

  base_link+=${tigervnc_version}
  base_link+="/tigervnc-"
  base_link+=${tigervnc_version}
  base_link+="."
  base_link+=${architecture}
  base_link+=".tar.gz"
  echo ${base_link}
}


# Downloads the latest TigerVNC bin and installs it
# @param $1 - boolean flag to indicate whether or not to run the function in verbose mode
# @param $2 - Bit type of the operating system currently running
function install_vnc() {
  output=$(determine_output $1)
  safe_download ${output} "tiger_vnc.tar.gz" $(get_tigervnc_download_link ${output} ${2})
  tar -zxf tiger_vnc.tar.gz --strip 1 -C /  &> $output
  rm -f tiger_vnc.tar.gz

  if [[ -f /usr/libexec/vncserver && ! -f /usr/bin/vncserver ]] ; then
    ln -s /usr/libexec/vncserver /usr/bin/vncserver
  fi
}

# Sets up tiger vnc for practical use
# @param $1 - boolean flag to indicate whether or not to run the function in verbose mode
# @param $2 - Port number to run the vnc server on
# @param $3 - Name of user to run VNC under
# @param $4 - Password string to use for logging in to VNC
# @param $5 - Name of the operating system currently running on (centos, debian or ubuntu)
# @param $6 - Version of the operating system we're currently using
# @return - None
function setup_vnc() {
  output=$(determine_output $1)
  port=$2
  name=$3
  password=$4
  operating_system=$5
  operating_system_version=$6

  mkdir /home/$name/.vnc
  echo $password >/home/$name/.vnc/file #TODO: See if we can pipe those together.
  vncpasswd -f </home/$name/.vnc/file >/home/$name/.vnc/passwd
  rm /home/$name/.vnc/file
  chown $name /home/$name/.vnc &> $output
  chown $name /home/$name/.vnc/passwd &> $output
  chmod 600 /home/$name/.vnc/passwd &> $output

  su  - $name -c "vncserver" &> $output
  su  - $name -c "vncserver -kill :1" &> $output

  echo -e '#!/usr/bin/env bash\n\nopenbox-session &' > "/home/$name/.vnc/xstartup"
  chmod +x /home/$name/.vnc/xstartup
  sed -i "s/$vncPort = 5900/$vncPort = $port - 1/g" /usr/bin/vncserver

  if [[ ${operating_system} == "centos" ]]; then
    echo "VNCSERVERS=\"1:$name\"" >> /etc/sysconfig/vncservers
    echo "VNCSERVERARGS[1]=\"-geometry 1024x786\"" >> /etc/sysconfig/vncservers

    add_tcp_firewall_rule ${output} ${operating_system} ${port} "vnc_port_t"

    if [[ ${operating_system_version} -ge 8 ]] ; then
      setup_vnc_systemd_service ${output} ${name}
      return
    fi
  fi

  setup_vnc_initd_service $output $name $operating_system
}

# Function to download a file using curl or wget depending on what is installed. This ensures better reliability across
# different platforms, as some OSs come with curl while others come with wget
# @param $1 - String where command output will be sent
# @param $2 - Name to save the downloaded file under
# @param $3 - URL to download the file from
# @return - None
function safe_download {
  output=$1
  output_filename=$2
  url=$3

  if [[ -x "$(which wget)" ]] ; then
    wget --no-check-cert -O ${output_filename} ${url} &> ${output}
  elif [[ -x "$(which curl)" ]] ; then
    curl -kLo ${output_filename} ${url} &> ${output}
  fi
}

# Function to add a firewall rule to allow incoming connections on x port
# @param $1 - String where the command outputs will be sent
# @param $2 - String containing the name of the Operating System that is running
# @param $3 - Port number
# @param $4 - Optional name for the rule being added, only used for selinux
# @return - None
function add_tcp_firewall_rule {
  output=$1
  operating_system=$2
  port_number=$3
  optional_rule_name=$4

  if [[ ${operating_system} == "centos" ]]; then
    if [[ -x "$(which firewall-cmd)" ]] ; then
      firewall-cmd --zone=public --add-port=${port_number}/tcp --permanent &> ${output}
      firewall-cmd --reload &> $output
    elif [[ -x "$(which semanage)" ]] ; then
      semanage port -a -t ${optional_rule_name} -p tcp ${port_number} &> $output
    fi
  fi
}
