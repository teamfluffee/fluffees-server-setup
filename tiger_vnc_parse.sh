#!/usr/bin/env bash

function get_tigervnc_version() {
  local temp_file="/tmp/tigervnc_version.tmp"

  wget -O ${temp_file} https://sourceforge.net/projects/tigervnc/files/stable/
  sed -i '/\/projects\/tigervnc\/files\/stable\//!d' ${temp_file}
  sed -i '/<th/!d' ${temp_file}
  sed -E -i 's/.*([0-9]+\.[0-9]+\.[0-9]+).*/\1/' ${temp_file}
  echo $(sort -n -t '.' -k 2 ${temp_file} | tail -1 && rm -f ${temp_file})
}

function get_tigervnc_download_link() {
  if [[ "$2" -eq 64 ]] ; then
    local architecture="x86_64"
  else
    local architecture="i386"
  fi

  local base_link="https://iweb.dl.sourceforge.net/project/tigervnc/stable/"
  local tigervnc_version=$(get_tigervnc_version)

  base_link+=${tigervnc_version}
  base_link+="/tigervnc-"
  base_link+=${tigervnc_version}
  base_link+="."
  base_link+=${architecture}
  base_link+=".tar.gz"
  echo ${base_link}
}

get_tigervnc_download_link hi 64

