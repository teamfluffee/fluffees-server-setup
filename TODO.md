# TODO
## Install PaleMoon to allow OSRS in a Browser
- Download PaleMoon (latest) from their website and install it (copy the extracted files)
- Download JDK 8u201 and install it, but not as the primary Java just install it
- Make a system link (`ln -s`) from the JDK 8 install folder with the Java plugin to `/usr/lib/mozilla/plugins/libjavaplugin.so`
- Load an OSRS game directly via HTTP (http://oldschool19.runescape.com)
- Profit
- Links to PaleMoon downloads: http://archive.palemoon.org/palemoon/28.x/28.9.3/
    - Use those downloads instead of the main site as they include a 32 bit linux download. The browser is unsafe but hey, so is running Java in a browser. We're already in sketchy territory
